// import * as bootstrap from 'bootstrap'


// Importing JavaScript
//
// You have two choices for including Bootstrap's JS files—the whole thing,
// or just the bits that you need.


// Option 1
//
// Import Bootstrap's bundle (all of Bootstrap's JS + Popper.js dependency)

// import "./../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";


// Option 2
//
// Import just what we need

// If you're importing tooltips or popovers, be sure to include our Popper.js dependency
// import "./.././../node_modules/popper.js/dist/popper.min.js";

// import "./.././../node_modules/bootstrap/js/dist/util.js";
// import "./.././../node_modules/bootstrap/js/dist/modal.js";
// import "./.././../node_modules/bootstrap/js/dist/dropdown.js"
// import "./.././../node_modules/bootstrap/js/dist/collapse.js"
// import "./.././../node_modules/bootstrap/js/dist/carousel.js"

// console.log('hello')

// const bsTab = new bootstrap.Tab('#myTab')

// const tabEl = document.querySelector('button[data-bs-toggle="tab"]')
// tabEl.addEventListener('shown.bs.tab', event => {
//   event.target // newly activated tab
//   event.relatedTarget // previous active tab
// })